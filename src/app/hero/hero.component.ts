import { Component, OnInit } from '@angular/core';
import { Hero } from '../../models/Hero';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.scss']
})
export class HeroComponent implements OnInit {

  public hero: Hero;

  public heroesList: Hero[]  = [
    {
        id: 1,
        name: 'Captain America',
        dead: false
    },
    {
        id: 2,
        name: 'Iron Man',
        dead: true
    },
    {
        id: 3,
        name: 'Hulk',
        dead: false
    },
    {
        id: 4,
        name: 'Thor',
        dead: false
    }
  ];

  constructor() { }

  ngOnInit() {
    this.hero = this.heroesList[0];
  }

  public setHighlight(hero: Hero) {
    this.hero = hero;
  }

  public ressHero(hero: Hero) {
    hero.dead = false;
  }

  public killHero(hero: Hero) {
    hero.dead = true;
  }
}
