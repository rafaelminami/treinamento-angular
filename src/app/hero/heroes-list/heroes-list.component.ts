import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { Hero } from '../../../models/Hero';


@Component({
  selector: 'app-heroes-list',
  templateUrl: './heroes-list.component.html',
  styleUrls: ['./heroes-list.component.scss']
})
export class HeroesListComponent implements OnInit {
  @Output() highlightEvent = new EventEmitter();

  @Input() heroes: Hero[] = [];

  constructor() { }

  ngOnInit() {
  }

  public setHighlight(hero: Hero) {
    this.highlightEvent.emit(hero);
  }
}
