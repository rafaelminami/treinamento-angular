export interface Hero {
    id: number;
    name: string;
    dead: boolean;
}
